var express = require('express');
var glob = require('glob');

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var swig = require('swig');
var passport = require('passport');
var session = require('express-session');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('express-flash');

module.exports = function(app, config) {
	app.engine('swig', swig.renderFile);
	app.set('views', config.root + '/app/views');
	app.set('view engine', 'swig');

	var env = process.env.NODE_ENV || 'development';
	app.locals.ENV = env;
	app.locals.ENV_DEVELOPMENT = env == 'development';

	// app.use(favicon(config.root + '/public/img/favicon.ico'));
	app.use(logger('dev'));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(cookieParser());
	app.use(compress());
	app.use(express.static(config.root + '/public'));
	app.use(methodOverride());

	//password middleware
	app.use(session({
		secret: 'keyboard cat',
		saveUninitialized: true,
		resave: true,
		cookie: {
			maxAge: 36000000
		}
	}));

	passport.use('local', new LocalStrategy(
		function(username, password, done) {

			console.log("LOGININFO:" + username + "," + password)

			var user = {
				id: '1',
				username: 'admin',
				password: 'password'
			}; // 可以配置通过数据库方式读取登陆账号

			if (username !== user.username) {
				return done(null, false, {
					message: 'Incorrect username.'
				});
			}
			if (password !== user.password) {
				return done(null, false, {
					message: 'Incorrect password.'
				});
			}

			return done(null, user);
		}
	));

	passport.serializeUser(function(user, done) { //保存user对象
		done(null, user); //可以通过数据库方式操作
	});

	passport.deserializeUser(function(user, done) { //删除user对象
		done(null, user); //可以通过数据库方式操作
	});

	app.use(passport.initialize());
	app.use(passport.session({
		maxAge: new Date(Date.now() + 3600000)
	}));

	app.use(flash());


	var controllers = glob.sync(config.root + '/app/controllers/*.js');
	controllers.forEach(function(controller) {
		require(controller)(app);
	});

	app.use(function(req, res, next) {
		var err = new Error('Not Found');
		err.status = 404;
		next(err);
	});

	if (app.get('env') === 'development') {
		app.use(function(err, req, res, next) {
			res.status(err.status || 500);
			res.render('error', {
				message: err.message,
				error: err,
				title: 'error'
			});
		});
	}

	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: {},
			title: 'error'
		});
	});

};
