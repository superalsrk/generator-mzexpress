var express = require('express');
var router = express.Router();
var passport = require('passport');
var checkauth = require('../helper/check-auth');

module.exports = function(app) {
	app.use('/', router);
};

router.get('/',checkauth, function(req, res, next) {
	res.render('index', {
		title: 'Generator-Express MVC',
	});
});
