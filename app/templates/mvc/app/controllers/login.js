var express = require('express');
var router = express.Router();
var passport = require('passport');

module.exports = function(app) {
	app.use('/account', router);
};

//load sign_in page
router.get('/sign_in', function(req, res, next) {
	res.render('login', {
		message: req.flash('error')
	});
});

//sign_in
router.post('/sign_in', passport.authenticate('local', {
	failureRedirect: '/account/sign_in',
	successRedirect: '/',
	failureFlash: true
}), function(req, res, next) {
	res.redirect('/');
});

//load sign_up page
router.get('/sign_up', function(req, res, next) {

});

//sign_up
router.post('/sign_up', function(req, res, next) {

});

//logout


router.get('/logout', function(req, res, next) {

});
