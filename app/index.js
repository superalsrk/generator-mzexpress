'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var util = require('util');
var path = require('path');
var _ = require('lodash');
var _s = require('underscore.string');

var Generator = module.exports = yeoman.generators.Base.extend({

	constructor: function() {
		yeoman.Base.apply(this, arguments);
	},

	initializing: function() {
		this.pkg = require('../package.json');
	},

	prompting: {
		askForAppName: function() {
			var done = this.async();

			this.log(yosay('Create your own ' + chalk.red('Yeoman') + ' generator with superpowers!'));

			this.log(this.appname);


			var prompts = [{
				name: 'appname',
				message: 'Your appname?',
				default: this.appname
			}];

			this.prompt(prompts, function(response) {
				this.appname = response.appname;
				done();
			}.bind(this));

		}
	},

	writing: {
		//Add basic info
		buildEnv: function() {
			this.log('buildENV');
			//this.sourceRoot(path.join(__dirname, 'templates', 'shared'));
			this.sourceRoot(path.join(__dirname, 'templates', 'shared'));
			this.expandFiles('**', {
				cwd: this.sourceRoot()
			}).map(function(file) {
				this.template(file, file.replace(/^_/, ''));
			}, this);

		},

		copyTheme: function() {
			this.log('copy theme');

			this.sourceRoot(path.join(__dirname, 'templates', 'assets'));
			this.directory('images', 'public/images');
			this.directory('javascripts', 'public/javascripts');
			this.directory('stylesheets', 'public/stylesheets');
		},

		copyMVC: function() {
			this.log('copy MVC');
			this.sourceRoot(path.join(__dirname, 'templates', 'mvc'));
			this.directory('app', 'app');
			this.directory('config', 'config');
			this.template('app.js', 'app.js');
		},


		app: function() {
			// this.fs.copy(
			// 	this.templatePath('shared/_package.json'),
			// 	this.destinationPath('package.json')
			// );
			// this.fs.copy(
			// 	this.templatePath('shared/_bower.json'),
			// 	this.destinationPath('bower.json')
			// );
			// this.fs.copy(
			// 	this.templatePath('shared/_bowerrc'),
			// 	this.destinationPath('.bowerrc')
			// )
		}
	},

	// assetsDirs: function() {
	// 	this.mkdir('public');
	// 	this.mkdir('public/components');
	// 	this.mkdir('public/js');
	// 	this.mkdir('public/css');
	// 	this.mkdir('public/img');
	// 	if (this.options.database == 'sqlite') {
	// 		this.mkdir('data');
	// 	}
	// },


	install: function() {
		this.installDependencies();
	}
});